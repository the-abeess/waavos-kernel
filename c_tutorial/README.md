# Bare Metal Raspberry Pi 3B+ basics with C

This directory holds the source files of some early attempt at waavos written
in C, greatly inspired by [this OSDev article][osdev-bare-metal-pi-c].

It is a *tiny bit* more complete, and is slightly different in some places.
It is mainly kept for archive and educational purposes.

[osdev-bare-metal-pi-c]: https://wiki.osdev.org/Raspberry_Pi_Bare_Bones
