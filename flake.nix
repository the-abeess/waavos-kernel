{
  inputs = {
    nci.url = "github:yusdacra/nix-cargo-integration";
    abeess = {
      url = "gitlab:the-abeess/libnix";
      inputs.parts.follows = "nci/parts";
    };
    devshell = {
      url = "github:numtide/devshell";
      inputs = { nixpkgs.follows = "nci/nixpkgs"; };
    };
  };

  outputs = inputs:
    inputs.abeess.lib.mkFlake
    { inherit inputs; }
    ({ libs, ... }: {
      systems = libs.systems.flakeExposed;
      imports = [ inputs.devshell.flakeModule inputs.nci.flakeModule ];

      nixpkgs.source = inputs.nci.inputs.nixpkgs;

      perSystem = { lib, pkgs, config, systemInfo, ... }: let

        inherit (lib) hiPrio;

        aarch64Pkgs = pkgs.pkgsCross.aarch64-embedded;
      
        mkEnvSuffix = name: suffix: "\${${name}:+\$${name}:}${suffix}";

      in {
        nci.projects.waavos = {
          path = ./.;
          export = false;
        };

        nci.crates.waavos  = {
          export = true;
        };

        devshells.default = {
          devshell.name = "WaavOS";
          devshell.packages = [
            pkgs.cargo-audit
            pkgs.cargo-binutils
            pkgs.cargo-bloat
            pkgs.cargo-deny
            pkgs.cargo-expand
            pkgs.cargo-feature
            pkgs.cargo-flamegraph
            pkgs.cargo-generate
            pkgs.cargo-llvm-cov
            pkgs.cargo-watch
            pkgs.gnumake
            pkgs.just
            pkgs.libllvm
            pkgs.lldb
            pkgs.pkg-config
            pkgs.binutilsNoLibc
          ];

          commands = [
            { package = pkgs.nixpkgs-fmt; category = "nix"; }
            {
              package = hiPrio config.nci.toolchains.shell;
              name = "cargo";
              help = pkgs.cargo.meta.description;
              category = "rust";
            }
            { category = "C"; package = hiPrio aarch64Pkgs.buildPackages.gcc; }
            {
              category = "tools";
              package = pkgs.qemu.overrideAttrs (prev: {
                # configureFlags = prev.configureFlags ++ [
                #   # Flags from https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials/blob/644474cc09f755249f9c55d99a5d1e07a2562fc7/docker/rustembedded-osdev-utils/Dockerfile#L62
                #   "--enable-modules"
                #   "--enable-tcg-interpreter"
                #   "--enable-debug-tcg"
                # ];
              });
            }
            {
              category = "tools";
              # libftdi should be enabled by default, but overriding costs
              # nothing if so anyway
              package = pkgs.openocd.override { enableFtdi = true; };
            }
            { category = "tools"; package = pkgs.hexyl; }
            { category = "tools"; package = pkgs.gdb; }
          ];

          env = [
            { name = "LIBRARY_PATH"; eval = "$DEVSHELL_DIR/lib"; }
            {
              name = "LD_LIBRARY_PATH";
              eval = mkEnvSuffix "LD_LIBRARY_PATH" "$DEVSHELL_DIR/lib";
            }
            {
              name = "C_INCLUDE_PATH";
              eval = mkEnvSuffix "C_INCLUDE_PATH" "$DEVSHELL_DIR/include";
            }
            {
              name = "PKG_CONFIG_PATH";
              eval = mkEnvSuffix "PKG_CONFIG_PATH" "$DEVSHELL_DIR/lib/pkgconfig";
            }
            { name = "CFLAGS"; eval = ''"-I $DEVSHELL_DIR/include"''; }
            {
              name = "RUST_SRC_PATH";
              eval = "$DEVSHELL_DIR/lib/rustlib/src/rust/library";
            }
            { name = "RUST_LOG"; value = "warn"; }
          ];
        };

      };
    });
}
