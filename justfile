prj_root := env("PRJ_ROOT", justfile_directory())

target_cpu := "cortex-a72"
target_arch := "aarch64"
target_vendor := "unknown"
target_env := "softfloat"
target := \
  target_arch + "-" + target_vendor + "-none" \
  + if target_env != "" { "-" + target_env } else { "" }

linker_path := "src"
linker_script := "waavos.ld"

rustc_target_flags := \
  "-C target-cpu=" + target_cpu
rustc_linker_flags := \
  "-C link-arg=--library-path=" + linker_path \
  + " -C link-arg=--script=" + linker_script

export RUSTFLAGS := env("RUSTFLAGS", "") \
  + " " + rustc_target_flags \
  + " " + rustc_linker_flags

bin_name := "waavos"
bin_path := prj_root / "target" / target / "debug" / bin_name 

qemu_bin := "qemu-system-" + target_arch
qemu_machine := "raspi3b"
qemu_cpu := target_cpu
qemu_serial := "stdio"
qemu_display := "none"

qemu_default_flags := "-d guest_errors"
qemu_flags := env("QEMU_FLAGS", qemu_default_flags) \
  + " -M " + qemu_machine \
  + " -cpu " + qemu_cpu \
  + " -kernel " + bin_path

# Lists available recipes
list:
  just --list

build:
    cargo build --target {{target}}

readobj *extra_flags: build
  cargo readobj --bin {{bin_name}} {{extra_flags}}

objdump *extra_flags: build
  rust-objdump --disassemble --demangle {{extra_flags}} {{bin_path}}

size *extra_flags:
  cargo size --bin {{bin_name}} --release -- -A {{extra_flags}}

emu *extra_flags: build
  {{qemu_bin}} -nographic -semihosting {{qemu_flags}} {{extra_flags}}

debug: build
  gdb -ex 'target remote localhost:1234' -q {{bin_path}}

