#![no_std]
#![no_main]
// FIXME: Check if all those features copied from the raspberrypi tutorial
//        are still necessary
// #![feature(asm_const)]
#![feature(format_args_nl)]
#![feature(panic_info_message)]

use core::arch::global_asm;
use core::cell::UnsafeCell;
use core::fmt::Write;
use core::marker::PhantomData;
use core::panic::PanicInfo;
use core::sync::atomic::{AtomicBool, Ordering};

use aarch64_cpu::asm;
// use qemu_exit::QEMUExit;
use semihosting::{println, process};

mod arch;

// #[macro_export]
// macro_rules! print {
//     ($($arg:tt)*) => ($crate::_print(format_args!($($arg)*)));
// }

// #[macro_export]
// macro_rules! println {
//     () => ($crate::print!("\n"));
//     ($($arg:tt)*) => ({
//         $crate::_print(format_args_nl!($($arg)*));
//     })
// }

global_asm!(include_str!("boot.s"));

/// Pause execution on the core.
#[inline(always)]
fn wait_forever() -> ! {
    loop {
        asm::wfe()
    }
}

// #[cfg_attr(not(test), panic_handler)]
#[cfg_attr(test, allow(dead_code))]
fn panic(info: &PanicInfo) -> ! {
    static NEVER_PANICKED: AtomicBool = AtomicBool::new(true);

    if NEVER_PANICKED.load(Ordering::Acquire) {
        let (location, line, column) = match info.location() {
            Some(loc) => (loc.file(), loc.line(), loc.column()),
            _ => ("???", 0, 0),
        };

        println!("Kernel panic !");
        println!();
        println!(
            "Panic location:\n      File '{}', line {}, column {}\n\n{}",
            location,
            line,
            column,
            info.message().unwrap_or(&format_args!("")),
        );
        println!();
        println!();
        println!("        * Don't stare into the abeess... *");
    }
    process::exit(0);
    // wait_forever();
}

struct QemuOuput {
    // Like it's making anything better /s
    _non_send: PhantomData<UnsafeCell<()>>,
}

impl core::fmt::Write for QemuOuput {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for c in s.bytes() {
            // I'm a wizard and I don't know what I'm doing
            // TODO: We should read again what the strict provenance write-up
            //       said about such dark volatile ptr magic but I think it's
            //       something along: (╯°□°)╯︵ ┻━┻
            unsafe { core::ptr::write_volatile(0x3F20_1000 as *mut u8, c) };
        }
        Ok(())
    }
}

#[doc(hidden)]
pub fn _print(args: core::fmt::Arguments) {
    term().unwrap().write_fmt(args).unwrap()
}

pub fn term() -> Option<impl core::fmt::Write> {
    Some(QemuOuput {
        _non_send: PhantomData,
    })
}

#[no_mangle]
pub unsafe fn _start_rust() -> ! {
    crate::kernel_init()
}

/// Early init code.
///
/// # Safety
///
/// - Only a single core must be active and running this function.
unsafe fn kernel_init() -> ! {
    // println!("hello from rust !");
    // panic!("Kernel panic\n\n        * Don't into the abeess... *\n");
    // qemu_exit::AArch64::new().exit_success();
    process::exit(0);
}
